//window.onload = function(){
    var dropZone = document.getElementById('file-loading-form');
    var errorContainer = dropZone.childNodes[3];
    var fileField = dropZone.childNodes[1].childNodes[3];
    var fileFieldLabel = dropZone.childNodes[1].childNodes[5];
    var fileFieldLabelDefaulContent = fileFieldLabel.innerHTML;

    if (typeof(window.FileReader) == 'undefined') {
        errorContainer.innerHTML = 'You browser not support drag&drop file upload.';
        dropZone.classList.add("has-error");
    }

    dropZone.ondragover = function() {
        dropZone.classList.add("is-dragover");
        return false;
    };

    dropZone.ondragleave = function() {
        dropZone.classList.remove('is-dragover');
        return false;
    };
    function handleFileSelect (event) {

        dropZone.classList.remove('is-dragover');
        dropZone.classList.add('is-drop');

        var file = fileField.files[0];
        fileFieldLabel.innerHTML = file.name;
    }

    dropZone.ondrop = function(event) {
        event.preventDefault();

        dropZone.classList.remove('is-dragover');
        dropZone.classList.add('is-drop');

        var file = event.dataTransfer.files[0];
        fileField.files = event.dataTransfer.files;
        fileFieldLabel.innerHTML = file.name;
    };

    fileField.addEventListener('change', handleFileSelect, false);
//};


