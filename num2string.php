<?php
/**
 * Числа
 *
 * числа ['один', 'одна'] и ['два', 'две'] имеют разные склонения в завсимости от названия степеней тысячи
 */
$numNames = [
    ['', ['один', 'одна'], ['два', 'две'], 'три', 'четыре', 'пять', 'шесть', 'семь', 'восемь', 'девять'],
    [
        '',
        ['десять','одиннадцать','двенадцать','тринадцать','четырнадцать' ,'пятнадцать','шестнадцать','семнадцать','восемнадцать','девятнадцать'],
        'двадцать', 'тридцать', 'сорок', 'пятьдесят', 'шестьдесят', 'семьдесят', 'восемьдесят', 'девяносто'
    ],
    ['', 'сто', 'двести', 'триста', 'четыреста', 'пятьсот', 'шестьсот', 'семьсот', 'восемьсот', 'девятьсот'],
];

/**
 * Именные названия степеней тысячи со склонением
 * 10^3 - 10^33
 */
$grandNumNames = [
    null,
    ['тысяча'  ,'тысячи'  ,'тысяч'],
    ['миллион' ,'миллиона','миллионов'],
    ['миллиард','милиарда','миллиардов'],
    ['триллион', 'триллиона', 'триллионов'],
    ['квадриллион', 'квадриллиона', 'квадриллионов'],
    ['квинтиллион', 'квинтиллиона', 'квинтиллионов'],
    ['секстиллион', 'секстиллиона', 'секстиллионов'],
    ['септиллион', 'септиллиона', 'септиллионов'],
    ['октиллион', 'октиллиона', 'октиллионов'],
    ['нониллион', 'нониллиона', 'нониллионов'],
    ['дециллион', 'дециллиона', 'дециллионов']
];

/**
 * @param $num
 * @return int $key of $grandNumNames
 */
function _getGrandNameKey($num){
    $num = (int)$num;

    if( ($num === 1) || ($num > 19  && ( ($num%10 === 1) && ($num%100 < 11 || $num%100 > 20) ) ) )
        return 0;

    if( ($num > 1 && $num < 5) || ( ($num%100 > 21) && ($num%10 > 1 && $num%10 < 5) ) )
        return 1;

    return 2;
}

/**
 * Формирует массив чисел прописью для трехзначных чисел
 * если передан ключ названия степени тысячи($grandNumNamesKey), то добавляет его в конец
 *
 * @param $num
 * @param null $grandNumNamesKey
 * @return array
 */
function _numToArrayStrings($num, $grandNumNamesKey = null){
    global $numNames, $grandNumNames;;

    $grandNameKey = _getGrandNameKey($num);
    $grandName = ( $num > 0 && isset($grandNumNames[$grandNumNamesKey][$grandNameKey]) ) ? $grandNumNames[$grandNumNamesKey][$grandNameKey] : '';

    // для чисел от 10 до 19
    if((int)$num >= 10 && (int)$num <= 19)
        return [ $numNames[1][1][$num%10], $grandName ];

    // для чисел от *10 до *19, где * сотни
    if($num%100 >= 10 && $num%100 <= 19)
        return [ $numNames[2][(string)$num[0]], $numNames[1][1][$num%10], $grandName ];

    // для всех остальных чисел
    $tempArr = [];

    /**
     * $i уровень чисел из массива $numNames
     * $j порядковый номер цифры в числе
     */
    for($i = (strlen( (string)$num ) - 1),  $j = 0; $i >= 0; $i--,  $j++){
        // выбираем нужные склонения для чисел ['один', 'одна'] и ['два', 'две']
        if( ($i === 0) && ((int)$num[$j] === 1 || (int)$num[$j] === 2) && is_array($numNames[$i][$num[$j]]) ){
            if( (int)$num[$j] === 1 && $grandNumNamesKey === 1
                || (int)$num[$j] === 2 && $grandNumNamesKey === 1
            )
                $tempArr[] = $numNames[$i][$num[$j]][1];
            else
                $tempArr[] = $numNames[$i][$num[$j]][0];
        }
        else
            $tempArr[] = $numNames[$i][$num[$j]];
    }
    $tempArr[] = $grandName;
    return $tempArr;
}

/**
 * Формирует число прописью
 *
 * @param $num
 * @return string
 */
function num2string($num){
    list($numbers, $dec) = explode('.', number_format((int)$num, 2, '.', ' '));
    $numbers = explode(' ', $numbers);

    $tempArr = [];

    /**
     * $i уровень названия степени тысячи $grandNumNames
     * $j порядковый номер тысячного разбиения в числе
     */
    for($i = (count($numbers) - 1),  $j = 0; $i >= 0; $i--,  $j++)
        $tempArr = array_merge( $tempArr, _numToArrayStrings($numbers[$j], $i) );

    foreach ($tempArr as $key=>$value)
        if($tempArr[$key] === '')
            unset($tempArr[$key]);

    return implode(' ', $tempArr);
}