<?php
ini_set("display_errors",1);
error_reporting(E_ALL);

include_once 'num2string.php';

$fileFieldName = 'userFile';

/**
 * Проверяет загружен ли файл с указанным именем поля
 *
 * @param $fieldName
 * @return bool
 */
function checkSendingFile($fieldName){
    return ( isset($_FILES[$fieldName]) && !empty($_FILES[$fieldName]) );
}

/**
 * Конвертирует строку в кодировку  UCS-2LE
 *
 * @param $str
 * @return string
 */
function convertStr2usc2le($str){
    // заменяем \n на \r\n для Windows Excel
    $str = str_replace("\n","\r\n", $str);
    return chr(255).chr(254).mb_convert_encoding($str, "UCS-2LE");
}

/**
 * Загружает файл пользователю
 *
 * @param $file
 * @throws Exception
 */
function uploadCsvFile($file) {
    if(!file_exists($file))
        throw new Exception("The file for upload not exist.");

    // сбрасываем буфер вывода PHP, чтобы избежать переполнения памяти выделенной под скрипт
    // если этого не сделать файл будет читаться в память полностью!
    if (ob_get_level()) {
        ob_end_clean();
    }

    // читаем файл и отправляем его
    $handle = fopen($file, 'rb');
    if(!$handle)
        throw new Exception("Failed to open the file for upload.");

    // заставляем браузер показать окно сохранения файла
    header('Content-Description: File Transfer');
    header('Content-Type: text/csv');
    header('Content-Disposition: attachment; filename=num2string.csv');// . basename($file));
    header('Content-Transfer-Encoding: binary');
    header('Expires: 0');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
    header('Content-Length: ' . filesize($file));

    while (!feof($handle)) {
        print convertStr2usc2le(fread($handle, 1024));
    }
    fclose($handle);
}

/**
 * Создает временный csv файл с двумя колонками: число из текстового файла и то же число прописью
 *
 * @param $file
 * @return string
 * @throws Exception
 */
function createCsvTempFile($file){
    if(!file_exists($file))
        throw new Exception("The downloaded file not load.");

    $tempFile  = tempnam(sys_get_temp_dir(), 'csv');
    if(!$tempFile)
        throw new Exception("Could not create the temp file.");

    if(!file_exists($tempFile))
        throw new Exception("The temp file not exist.");

    $handle  = fopen($file, "r");
    if(!$handle)
        throw new Exception("Failed to open the downloaded file.");

    $handleTempFile  = fopen($tempFile, "w+");
    if(!$handleTempFile)
        throw new Exception("Failed to open the temp file.");

    while($line=fgets($handle)) {
        fputcsv($handleTempFile, [(int)$line, num2string($line)], "\t");
    }
    fclose($handle);
    fclose($handleTempFile);

    return $tempFile;
}

$error = false;
$errorStr = '';

if(checkSendingFile($fileFieldName)){
    try{
        $downloadedFile = $_FILES[$fileFieldName]['tmp_name'];

        $csvTempFile = createCsvTempFile($downloadedFile);

        uploadCsvFile($csvTempFile);

        unlink($csvTempFile);

        exit;
    }
    catch (Exception $e){
        $error = true;
        $errorStr = $e->getMessage();
    }
}

?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Numbers to String Converter</title>

    <link rel="stylesheet" href="main.css">
</head>
<body>
<div id="app">

    <form id="file-loading-form" class="<?= $error ? ' has-error' : ''?>" enctype="multipart/form-data" method="post" action="/">
        <div class="file-field-container">
            <svg class="download-icon" xmlns="http://www.w3.org/2000/svg" width="50" height="43" viewBox="0 0 50 43"><path d="M48.4 26.5c-.9 0-1.7.7-1.7 1.7v11.6h-43.3v-11.6c0-.9-.7-1.7-1.7-1.7s-1.7.7-1.7 1.7v13.2c0 .9.7 1.7 1.7 1.7h46.7c.9 0 1.7-.7 1.7-1.7v-13.2c0-1-.7-1.7-1.7-1.7zm-24.5 6.1c.3.3.8.5 1.2.5.4 0 .9-.2 1.2-.5l10-11.6c.7-.7.7-1.7 0-2.4s-1.7-.7-2.4 0l-7.1 8.3v-25.3c0-.9-.7-1.7-1.7-1.7s-1.7.7-1.7 1.7v25.3l-7.1-8.3c-.7-.7-1.7-.7-2.4 0s-.7 1.7 0 2.4l10 11.6z"></path></svg>
            <input id="field-<?= $fileFieldName?>" class="field-input" name="<?= $fileFieldName?>" class="form-field" type="file"/>
            <label for="field-<?= $fileFieldName?>" class="field-label"><strong>Choose a file</strong><span class="box__dragndrop"> or drag it here</span>.</label>
            <button class="submit-button" type="submit">Upload</button>
        </div>

        <div class="error-container">
            <?= $error ? $errorStr : ''?>
        </div>
    </form>
</div>

<script type="text/javascript" src="main.js"></script>

</body>

</html>
